﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Vision : MonoBehaviour 
{
	public float blindSpotAngle;
	public float distance;


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	bool IsPlayerDangerous(Player player)
	{
		float angle = Vector3.Angle(transform.forward, (player.transform.position - transform.position));
		float d = Vector3.Distance(transform.position, player.transform.position);
		return d < distance &&	Mathf.Abs(angle) < (180 - blindSpotAngle);
	}

	public Player[] FindPlayersWithinVision()
	{
		return GameObject.FindObjectsOfType<Player>()
			.Where (e => IsPlayerDangerous(e))
			.OrderBy (e => Vector3.Distance (transform.position, e.transform.position) )
			.ToArray ();
	}
}
