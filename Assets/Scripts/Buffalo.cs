﻿using UnityEngine;
using System.Collections;

public class Buffalo : MonoBehaviour 
{
	public enum State
	{
		Idle,
		Move,
		Angry,
	}

	// Tweakable
	public float idleTimeMin = 2f;
	public float idleTimeMax = 5f;
	public float idleTurnMax = 180.0f;
	public float turnSpeed = 90;
	public float moveTimeMin = 1f;
	public float moveTimeMax = 2f;
	public float moveSpeed = 2f;
	public float chargeTime = 0.5f;
	public float chargeSpeed = 30f;

	public float lookTimeout = 0.5f;
	public float nextLookTime = 0f;

	// State
	public State currentState = State.Idle;
	public float taskEndTime = 0f;

	public Color color;

	private Vision vision;

	// Use this for initialization
	void Start () 
	{
		nextLookTime = Time.time + nextLookTime + Random.value;

		foreach (var renderer in GetComponentsInChildren<Renderer>())
		{
			if (renderer.material != null)
				renderer.material.color = color;
		}

		vision = GetComponent<Vision>();
	}

	void CheckForPlayers()
	{
		var players = vision.FindPlayersWithinVision();

		if (players.Length > 0)
		{
			currentState = State.Angry;
			taskEndTime = Time.time + chargeTime;

			var direction = (players[0].transform.position - transform.position).normalized;
			var angle = Vector3.Angle(transform.forward, direction);
			//transform.rotation.SetLookRotation(direction, transform.up);
			transform.Rotate(0.0f, angle, 0.0f);

			//Debug.Log("Running Away!", gameObject);
		}
	}

	void Update()
	{
		float currentTime = Time.time;

		// Update scouting only intermittently
		if (currentTime > nextLookTime && currentState != State.Angry)
		{
			CheckForPlayers();
			nextLookTime = currentTime + lookTimeout;
		}

		if (currentState == State.Idle)
		{
			if (currentTime > taskEndTime)
			{
				currentState = State.Move;
				taskEndTime = currentTime + Random.Range(moveTimeMin, moveTimeMax);

				var turn = Random.Range(-idleTurnMax, idleTurnMax);
				transform.Rotate(0.0f, turn, 0.0f);

				//Debug.Log("Moving for greener glass.", gameObject);
			}
		}
		else
		{
			if (currentTime > taskEndTime)
			{
				currentState = State.Idle;
				taskEndTime = currentTime + Random.Range(idleTimeMin, idleTimeMax);

				//Debug.Log("Back to Munching", gameObject);
			}

			float speed = (currentState == State.Move) ? moveSpeed : chargeSpeed;
			//transform.position = transform.position + transform.forward.normalized * speed * Time.deltaTime;
			var movement = transform.forward * speed * Time.deltaTime;
			rigidbody.MovePosition(rigidbody.position + movement);
		}
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(1, 1, 0, 0.3f);
		Gizmos.DrawWireSphere(transform.position, vision.distance);

		Gizmos.color = Color.white;
		Gizmos.DrawLine(transform.position, transform.position + transform.forward * 10);
	}
}
