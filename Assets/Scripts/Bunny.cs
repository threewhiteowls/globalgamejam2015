﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;

public class Bunny : MonoBehaviour
{
	public enum State
	{
		Idle,
		Move,
		Run,
	}

	// Tweakable
	//public float visionBlindSpotAngle = 45f;
	//public float visionDistance = 10f;
	public float idleTimeMin = 2f;
	public float idleTimeMax = 5f;
	public float idleTurnMax = 180.0f;
	public float turnSpeed = 180;
	public float moveTimeMin = 1f;
	public float moveTimeMax = 3f;
	public float moveSpeed = 5f;
	public float runTime = 0.5f;
	public float runSpeed = 40f;

	public float lookTimeout = 0.5f;
	public float nextLookTime = 0f;

	// State
	public State currentState = State.Idle;
	public float taskEndTime = 0f;

	public Color[] colours;

	private Vision vision;

	void Start ()
	{
		nextLookTime = Time.time + nextLookTime + Random.value;

		int colorIndex = Random.Range(0, colours.Length);
		foreach (var renderer in GetComponentsInChildren<Renderer>())
		{
			if (renderer.material != null)
				renderer.material.color = colours[colorIndex];
		}

		vision = GetComponent<Vision>();
	}

	void CheckForPlayers()
	{
		var players = vision.FindPlayersWithinVision();

		if (players.Length > 0)
		{
			currentState = State.Run;
			taskEndTime = Time.time + runTime;

			var direction = (transform.position - players[0].transform.position).normalized;
			var angle = Vector3.Angle(transform.forward, direction);
			//transform.rotation.SetLookRotation(direction, transform.up);
			transform.Rotate(0.0f, angle, 0.0f);

			//Debug.Log("Running Away!", gameObject);
		}
	}

	void Update ()
	{
		float currentTime = Time.time;

		// Update scouting only intermittently
		if (currentTime > nextLookTime && currentState != State.Run)
		{
			CheckForPlayers();
			nextLookTime = currentTime + lookTimeout;
		}
		
		if (currentState == State.Idle)
		{
			if (currentTime > taskEndTime)
			{
				currentState = State.Move;
				taskEndTime = currentTime + Random.Range(moveTimeMin, moveTimeMax);

				var turn = Random.Range(-idleTurnMax, idleTurnMax);
				transform.Rotate(0.0f, turn, 0.0f);

				//Debug.Log("Moving for greener glass.", gameObject);
			}
		}
		else
		{
			if (currentTime > taskEndTime)
			{
				currentState = State.Idle;
				taskEndTime = currentTime + Random.Range(idleTimeMin, idleTimeMax);

				//Debug.Log("Back to Munching", gameObject);
			}

			float speed = (currentState == State.Move) ? moveSpeed : runSpeed;
			//transform.position = transform.position + transform.forward.normalized * speed * Time.deltaTime;
			var movement = transform.forward * speed * Time.deltaTime;
			rigidbody.MovePosition(rigidbody.position + movement);
		}
	}

	void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(1, 1, 0, 0.3f);
		Gizmos.DrawWireSphere(transform.position, vision.distance);

		Gizmos.color = Color.white;
		Gizmos.DrawLine(transform.position, transform.position + transform.forward * 10);
	}
}