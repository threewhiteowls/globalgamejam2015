﻿using UnityEngine;
using System.Collections;

public class YawRotation : MonoBehaviour
{
	public float rotationSpeed = 30;

	// Update is called once per frame
	void Update()
	{
		transform.eulerAngles = transform.eulerAngles +
			new Vector3(0, rotationSpeed*Time.deltaTime, 0);
	}
}
