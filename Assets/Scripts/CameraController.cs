﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour
{
	public float minDistance;
	public float groundOffset;
	public float pitchAngle;
	public float yawAngle;
	public float positionChangeSpeed;
	public float distanceChangeSpeed;

	public float currentDistance;
	public Vector3 currentPosition;

	// Use this for initialization
	void Start ()
	{
		currentDistance = minDistance;
		currentPosition = Vector3.zero;
		pitchAngle = 30.0f;
		yawAngle = 0f;
	}

	// Update is called once per frame
	void Update ()
	{
		var players = GameController.instance.players;
		if (players.Count == 0)
			return;

		Vector3 minimum = players[0].transform.position;
		Vector3 maximum = minimum;
		foreach (var p in players)
		{
			minimum = Vector3.Min(minimum, p.transform.position);
			maximum = Vector3.Max(maximum, p.transform.position);
		}

		Vector3 center = (maximum + minimum) / 2;
		float maxDimension = Mathf.Max(maximum.x - minimum.x, maximum.z - minimum.z);

		var targetPos = currentPosition;
		var targetDist = currentDistance;
		
		if (GameController.instance.isPlaying)
		{
			targetPos = center;
			targetDist = Mathf.Max(maxDimension, minDistance);
		}

		currentPosition = Vector3.MoveTowards(currentPosition, targetPos, positionChangeSpeed*Time.deltaTime);
		currentDistance = Mathf.MoveTowards(currentDistance, targetDist, distanceChangeSpeed*Time.deltaTime);

		var angles = transform.eulerAngles;
		angles.y = yawAngle;
		angles.x = pitchAngle;

		transform.eulerAngles = angles;
		transform.position = targetPos + Vector3.up*groundOffset - transform.forward*currentDistance;
	}
}
