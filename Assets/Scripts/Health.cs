﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour 
{
	public float fullHealth;
	public float health;

	// Use this for initialization
	void Start () 
	{
		health = fullHealth;
	}

	public string Description
	{
		get
		{
			if (health >= fullHealth) return "Healthy";
			if (health >= fullHealth / 3) return "Alive";
			return "Dead";
		}
	}

	public bool Attack(float damage)
	{
		health -= damage;
		gameObject.SendMessage("OnDamaged", SendMessageOptions.DontRequireReceiver);
		if (health <= 0.0f)
		{
			gameObject.SendMessage("OnDeath", SendMessageOptions.DontRequireReceiver);
			return true;
		}
		return false;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
