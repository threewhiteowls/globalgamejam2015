﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerInteractions : MonoBehaviour
{
	public List<GameObject> touchingObjects = new List<GameObject>();

    void Awake()
    {
    }

    void Start()
    {
    }

    void Update()
    {
    }

	void OnTriggerEnter(Collider other)
	{
		if (other.isTrigger && !touchingObjects.Contains(other.gameObject))
			touchingObjects.Add(other.gameObject);
	}

	void OnTriggerExit(Collider other)
	{
		touchingObjects.Remove(other.gameObject);
	}
}
