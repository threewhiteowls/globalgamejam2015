﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GameController : MonoBehaviour
{
	public static GameController instance;

	[Serializable]
	public struct SpawnPoint
	{
		public Transform location;
		public Color color;
	}

	// Config
	public GameObject playerPrefab;
	public SpawnPoint[] spawnPoints;

	// Runtime data
	public List<Player> players;
	public int playerCount = 1;
	public bool isPlaying = false;
	public bool isOver = false;

	void Awake()
	{
		instance = this;
	}

	// Use this for initialization
	void Start()
	{
	}
	
	// Update is called once per frame
	void Update ()
	{
	}

	void StartGame()
	{
		var maxPlayers = Mathf.Min(spawnPoints.Length, playerCount, 4);

		players = new List<Player>();

		for (var i = 0; i < maxPlayers; i++)
		{
			var transform = spawnPoints[i].location.transform;
			var color = spawnPoints[i].color;

			// Spawning player
			var pos = Utils.RaycastFloor(transform.position);
			var go = GameObject.Instantiate(playerPrefab, pos + Vector3.up*0.05f,
			                                transform.rotation) as GameObject;

			var player = go.GetComponent<Player>();

			player.color = color;
			go.SetActive(true);
			players.Add(player);
			player.playerIndex = players.Count;

			// Changing its color
			foreach (var renderer in go.GetComponentsInChildren<Renderer>())
			{
				if (renderer.material != null)
					renderer.material.color = color;
			}
		}

		isPlaying = true;
	}

	void OnGUI ()
	{
		GUI.matrix = Matrix4x4.Scale(new Vector3(1, 1, 0.99f)*(Screen.height/320.0f));

		if (!isPlaying)
		{
			DrawStartScreen();
		}
		else if (isOver)
		{
			DrawEndScreen();
		}
		else
		{
			DrawGameScreen();
		}
	}

	void DrawStartScreen()
	{
		var labelWidth = 150;

		GUI.Label(new Rect(10, 10, labelWidth, 20), "Players: " + playerCount);

		if (GUI.Button(new Rect (labelWidth - 50, 10, 20, 20), "-"))
			playerCount = Mathf.Max(1, playerCount - 1);

		if (GUI.Button(new Rect (labelWidth - 20, 10, 20, 20), "+"))
			playerCount = Mathf.Min(4, playerCount + 1);
		
		if (GUI.Button(new Rect (10, 40, labelWidth, 20), "Start Game"))
			StartGame();
	}

	void DrawEndScreen()
	{
		GUI.Label(new Rect(10, 10, 200, 20), "Game Over");

		if (GUI.Button(new Rect (10, 40, 120, 20), "Replay"))
			Application.LoadLevel(Application.loadedLevel);
	}

	void DrawGameScreen()
	{
		for (var i = 0; i < players.Count; i++)
		{
			var p = players[i];
			var info = "Player " + p.playerIndex + ":" + p.StatusText;

			GUI.color = p.color;
			GUI.color = new Color(p.color.r, p.color.g, p.color.b, 1.0f);

			GUI.Label(new Rect(10, 10 + i*20, 200, 30), info);
		}
	}
}
