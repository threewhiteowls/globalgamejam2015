﻿using UnityEngine;
using System.Collections;

public class DamageEffects : MonoBehaviour
{
	public GameObject hitEffectPrefab;
	public GameObject deathEffectPrefab;
	public bool deleteOnDeath;

	void PlayEffect(GameObject prefab)
	{
		var go = Instantiate(prefab, transform.position, transform.rotation) as GameObject;

		if (go != null)
		{
			foreach (var fx in go.GetComponentsInChildren<ParticleSystem>())
				fx.Play();
		}
	}

	void OnDamaged()
	{
		if (hitEffectPrefab != null)
			PlayEffect(hitEffectPrefab);
	}
	
	void OnDeath()
	{
		if (deathEffectPrefab != null)
			PlayEffect(deathEffectPrefab);

		if (deleteOnDeath)
			GameObject.Destroy(gameObject);
	}
}
