﻿using UnityEngine;
using System.Collections;

public class Food : MonoBehaviour {

	public float foodValue;

	// Use this for initialization
	void Start () {
	
	}

	public float Digest()
	{
		gameObject.SendMessage("OnEaten", SendMessageOptions.DontRequireReceiver);
		float value = foodValue;
		foodValue = 0f;
		return value;
	}

	// Update is called once per frame
	void Update () {
	
	}
}
