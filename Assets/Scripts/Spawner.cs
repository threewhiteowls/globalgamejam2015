﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {

	public int count = 5;
	public float radius;
	public GameObject entity;

	// Use this for initialization
	void Start () 
	{
		for (int i = 0; i < count; i++)
		{
			var pos = Random.insideUnitCircle * radius;
			Vector3 position = Utils.RaycastFloor(transform.position + new Vector3(pos.x, 0.0f, pos.y)) + Vector3.up * 0.05f;
			var rotation = Quaternion.Euler(0.0f, Random.Range(0.0f, 360.0f), 0.0f);

			var o = Instantiate(entity, position, rotation) as GameObject;
			o.SetActive(true);
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	void OnDrawGizmosSelected()
	{
		Gizmos.color = new Color(1.0f, 1.0f, 0, 1.0f);
		Gizmos.DrawWireSphere(transform.position, radius);
	}
}
