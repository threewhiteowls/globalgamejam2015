﻿using UnityEngine;
using System.Collections;

public class Utils
{
	public static Vector3 RaycastFloor(Vector3 pos)
	{
		var ray = new Ray(new Vector3(pos.x, 50, pos.z), Vector3.down);
		RaycastHit hit;

		if (Physics.Raycast(ray, out hit, 150, 1 << LayerMask.NameToLayer("Terrain")))
			return hit.point;

		return pos;
	}
}
