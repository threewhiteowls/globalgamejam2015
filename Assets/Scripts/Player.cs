﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class Player : MonoBehaviour
{
	public int playerIndex;
	public Color color;

	public Vector3 movement;
	public float rotation;
	public Vector3 direction;

	public float hunger = 0.0f;

	private const float walkingSpeed = 10.0f;
	private const float runningSpeed = 25.0f;
	private const float rotationSpeed = 90.0f;

	private bool isRunning = false;

	private const float hungerRateIdle = 0.5f;
	private const float hungerRateWalking = 1.5f;
	private const float hungerRateRunning = 3.0f;
	private const float hungerRateAttack = 5.0f;

	private const float hungerHungryLevel = 50.0f;
	private const float hungerStarvingLevel = 100.0f;
	private const float hungerDamageLevel = 150.0f;
	private const float hungerDamageAmount = 5.0f;

	public GameObject deathParticlesPrefab;

    void Awake()
    {
    }

    void Start()
	{
		direction = transform.forward;
    }

    void Update()
    {
        InputMovement();

		if (Input.GetButtonDown("Attack" + playerIndex))
			DoInteraction();

		ProcessHunger();
    }

	void ProcessHunger()
	{
		hunger += hungerRateIdle * Time.deltaTime;

		if (hunger > hungerDamageLevel)
		{
			var health = GetComponent<Health>();
			health.Attack((hunger - hungerDamageLevel) * Time.deltaTime);
		}
	}

	public string StatusText
	{
		get
		{
			var health = GetComponent<Health>();
			string result = health.Description + " ";

			if (hunger >= hungerDamageLevel) result += "Starvation!";
			else if (hunger >= hungerStarvingLevel) result += "Starving";
			else if (hunger >= hungerHungryLevel) result += "Hungry";
			//else result += "Well Fed";
			return result;
		}
	}

	void InputMovement()
    {
		var h = Input.GetAxisRaw("Horizontal" + playerIndex);
		var v = Input.GetAxisRaw("Vertical" + playerIndex);
		isRunning = Input.GetButton("Run" + playerIndex);

		var speed = isRunning ? runningSpeed : walkingSpeed;

		movement.Set(h, 0.0f, v);

		if (movement.sqrMagnitude < 0.01)
			return;

		direction = movement.normalized;
		rigidbody.MovePosition(rigidbody.position + movement * Time.deltaTime * speed);

		transform.rotation = Quaternion.LookRotation(direction, transform.up);

		if (movement.sqrMagnitude > 0.0f)
		{
			hunger += (isRunning ? hungerRateRunning : hungerRateWalking) * Time.deltaTime;
		}
		else
		{
			hunger += hungerRateIdle * Time.deltaTime;
		}
    }

	private float interactAngle = 30.0f;
	private float interactDistance = 10.0f;
	private float interactDamage = 30.0f;

	bool CanInteract(GameObject o)
	{
		float angle = Vector3.Angle(transform.forward, (o.transform.position - transform.position));
		float distance = Vector3.Distance(transform.position, o.transform.position);
		return distance < interactDistance && Mathf.Abs(angle) < interactAngle;
	}

	void DoInteraction()
	{
		Debug.Log("Interacting");

		var thing = GameObject.FindObjectsOfType<Food>()
			.Where(e => CanInteract(e.gameObject))
			.OrderBy(e => Vector3.Distance(transform.position, e.transform.position))
			.FirstOrDefault();

		if (thing != null)
		{
			bool canEat = true;

			var thingHealth = thing.GetComponent<Health>();
			if (thingHealth != null)
			{
				canEat = thingHealth.Attack(interactDamage);
			}

			if (canEat)
			{
				hunger -= thing.Digest();
			}
		}

		hunger += hungerRateAttack;
	}

	void OnDeath()
	{
		GameController.instance.players.Remove(this);

		if (GameController.instance.players.Count == 0)
			GameController.instance.isOver = true;
	}
}
